const electron = require("electron");
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const path = require("path");
const url = require("url");
const { ipcMain } = require("electron");
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 480,
    height: 800,
    webPreferences: { nodeIntegration: true, contextIsolation: false },
  });

  // and load the index.html of the app.
  const startUrl = process.env.DEV
    ? "http://localhost:3000"
    : url.format({
        pathname: path.join(__dirname, "/../build/index.html"),
        protocol: "file:",
        slashes: true,
      });
  mainWindow.loadURL(startUrl);
  // Open the DevTools.

  mainWindow.webContents.openDevTools();

  // Set fullscreen
  mainWindow.setFullScreen(true);
  
  // Emitted when the window is closed.
  mainWindow.on("closed", function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// ------------------- set up event listeners here --------------------

// temporary variable to store data while background
// process is ready to start processing
let cache = {
  data: undefined,
};

// a window object outside the function scope prevents
// the object from being garbage collected
let hiddenWindow, eventControlHiddenWindow;

// This event listener will listen for request
// from visible renderer process
ipcMain.on("START_BACKGROUND_VIA_MAIN", (event, args) => {
  const backgroundFileUrl = url.format({
    pathname: path.join(__dirname, `/background_tasks/background.html`),
    protocol: "file:",
    slashes: true,
  });
  hiddenWindow = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  hiddenWindow.loadURL(backgroundFileUrl);

  hiddenWindow.webContents.openDevTools();

  hiddenWindow.on("closed", () => {
    hiddenWindow = null;
  });
});

// This event listener will listen for data being sent back
// from the background renderer process
ipcMain.on("MESSAGE_FROM_BACKGROUND", (event, args) => {
  mainWindow.webContents.send("MESSAGE_FROM_BACKGROUND_VIA_MAIN", args.message);
});

ipcMain.on("BACKGROUND_READY", (event, args) => {
  event.reply("START_PROCESSING", {
  });
});

ipcMain.on("SEND_OPIONS", (event, args) => {
  hiddenWindow.webContents.send("SEND_TEST",{data:args.data});
})