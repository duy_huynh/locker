const net = require("net");
const port = process.env.PORT ? process.env.PORT - 100 : 3001;

process.env.ELECTRON_START_URL = `http://localhost:${port}`;

const client = new net.Socket();

let startedElectron = false;
const tryConnection = () =>
  client.connect({ port: port }, () => {
    client.end();
    if (!startedElectron) {
      startedElectron = true;
      const exec = require("child_process").exec;
      const electron = exec("npm run electron");
      electron.stdout.on("data", function (data) {
        console.log("stdout: " + data.toString());
      });
    }
  });

tryConnection();

// eslint-disable-next-line no-unused-vars
client.on("error", (error) => {
  setTimeout(tryConnection, 1000);
});
