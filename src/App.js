import React, { Component } from "react";
import { MemoryRouter as Router, Route, Switch } from "react-router-dom";
import WrapperLayout from "./components/WrapperLayout";
import CONSTANTS from "./constants";
import { LanguageProvider } from "./context/LanguageContext";
import Confirm from "./pages/Confirm";
import Home from "./pages/Home";
import InputCode from "./pages/InputCode";
import LockerNumber from "./pages/LockerNumber";
import PhoneInput from "./pages/PhoneInput";
import ThanksPage from "./pages/ThanksPage";

class App extends Component {
  render() {
    return (
      <Router>
        <LanguageProvider>
          <WrapperLayout>
            <Switch>
              <Route exact path={CONSTANTS.ROUTERS.PHONE_INPUT}>
                <PhoneInput />
              </Route>
              <Route exact path={CONSTANTS.ROUTERS.CONFIRM}>
                <Confirm />
              </Route>
              <Route exact path={CONSTANTS.ROUTERS.INPUT_CODE}>
                <InputCode />
              </Route>
              <Route exact path={CONSTANTS.ROUTERS.LOCKER_NUMBER}>
                <LockerNumber />
              </Route>
              <Route exact path={`${CONSTANTS.ROUTERS.THANKS_PAGE}/:state`} children={<ThanksPage />} />
              <Route path={CONSTANTS.ROUTERS.HOME}>
                <Home></Home>
              </Route>
            </Switch>
          </WrapperLayout>
        </LanguageProvider>
      </Router>
    );
  }
}

export default App;
