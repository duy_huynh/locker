import React from "react";
import "./style.scss";

const InputPhone = ({ title, value, onChange }) => {
  return (
    <div className="inputphone__wrapper">
      <label className="inputphone__label" htmlFor="phonenumber">
        {title}
      </label>
      <input
        value={value}
        className="inputphone__phone"
        onChange={onChange}
        placeholder=""
      />
    </div>
  );
};

export default InputPhone;
