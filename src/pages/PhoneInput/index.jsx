import React, { useContext, useState } from "react";
import callElectron from "src/actions/CallElectron";
import CONSTANTS, { STATUS } from "src/constants";
import { TYPES } from "src/constants/actions/Control";
import { ControlContext } from "src/context/ControlContext";
import { LanguageContext } from "src/context/LanguageContext";
import ButtonSubmit from "./components/ButtonSubmit";
import InputPhone from "./components/InputPhone";
import KeyBoard from "./components/KeyBoard";
import "./style.scss";

const PhoneInput = () => {
  const [value, setValue] = useState("");
  const [validate, setValidate] = useState(true);
  const controlValue = useContext(ControlContext);
  const dispatch = controlValue[1];
  const {dictionary} = useContext(LanguageContext);
  // const onClickInput = () => {
  //   const listButton = document.getElementsByClassName("buttonoption__wrapper");
  //   if (listButton) {
  //     for (var i = 0; i < listButton.length; i++) {
  //       listButton[i].classList.add("buttonoption__bright");
  //     }
  //     setTimeout(() => {
  //       for (var i = 0; i < listButton.length; i++) {
  //         listButton[i].classList.remove("buttonoption__bright");
  //       }
  //     }, 200);
  //   }
  // };

  const handleGetCode = () => {
    callElectron(`${STATUS.OK} ${value}`);
    // wait code or error here then redirect or show log
    dispatch({type: TYPES.CALL_SERVICE,pageGo: CONSTANTS.ROUTERS.INPUT_CODE, isPush: true, isSending: true});
  };

  const handlePhone = (newNumber) => {
    if (newNumber === undefined || newNumber === "-1") {
      setValidate(true);
      setValue(value.slice(0, -1));
      return null;
    }

    if (value.length >= 10) {
      return null;
    }

    setValue(`${value}${newNumber}`);
    const phone_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    setValidate(!phone_regex.test(`${value}${newNumber}`));
  };

  const handleChangeValue = (e) => {
    const value = e.target.value;
    if (isNaN(value)) {
      return null;
    }
    if (value.length > 10) {
      return null;
    }

    setValue(value);
    const phone_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    setValidate(!phone_regex.test(value));
  };

  return (
    <div className="phoneinput__wrapper">
      <form className="phoneinput__form" onSubmit={(e) => e.preventDefault()}>
        <InputPhone
          title={dictionary.PHONE_INPUT.NUMBER_PHONE}
          value={value}
          onChange={handleChangeValue}
        ></InputPhone>
        <div className="phoneinput__button">
          <ButtonSubmit
            onClick={handleGetCode}
            value={dictionary.PHONE_INPUT.GET_CODE}
            disabled={validate}
          ></ButtonSubmit>
          <ButtonSubmit
            value={dictionary.PHONE_INPUT.CANCEL}
            onClick={() => {
              callElectron(STATUS.CANCEL);
              dispatch({type: TYPES.CALL_SERVICE,pageGo: CONSTANTS.ROUTERS.HOME, isPush: true, isSending: false});
            }}
          ></ButtonSubmit>
        </div>
        <KeyBoard
          onClick={(e) => {
            handlePhone(e.target.value);
          }}
        />
      </form>
    </div>
  );
};

export default React.memo(PhoneInput);
