import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import CONSTANTS from "src/constants";
import { ControlContext } from "src/context/ControlContext";
import Citynow from "../../images/logovn.png";
// import BackButton from "./components/BackButton";
import BoxNumber from "./components/BoxNumber";
import "./style.scss";

const LockerNumber = () => {
  const history = useHistory();
  const controlValue = useContext(ControlContext);
  useEffect(() => {
    if (isNaN(controlValue[0].lastReceive)){
      history.push(`${CONSTANTS.ROUTERS.THANKS_PAGE}/thanks`);
    }
  },[controlValue,history])
  return (
    <div className="lockernumber__wrapper">
      <img width="200" src={Citynow} alt="Citynow" />
      <h3 className="lockernumber__h3">Your number</h3>
      <BoxNumber value={controlValue[0].lastReceive} />
      <br />
      {/* <BackButton
        onClick={() => history.push(CONSTANTS.ROUTERS.HOME)}
        value="Back home"
      ></BackButton> */}
    </div>
  );
};

export default LockerNumber;
