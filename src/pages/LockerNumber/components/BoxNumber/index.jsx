import React from "react";
import "./style.scss";

const BoxNumber = ({ value }) => {
  return (
    <div className="boxnumber__wrapper">
      <p className="boxnumber__number">{value}</p>
    </div>
  );
};

export default BoxNumber;
