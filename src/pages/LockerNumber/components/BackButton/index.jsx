import React from "react";
import "./style.scss";

const BackButton = ({ value, onClick }) => {
  return (
    <button className="backbutton__wrapper" onClick={onClick}>
      {value}
    </button>
  );
};

export default BackButton;
