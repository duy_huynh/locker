import React from "react";
import MainLayout from "./components/MainLayout";
import "./style.scss";

const Home = () => {

  return (
    <div>
      <MainLayout></MainLayout>
    </div>
  );
};

export default Home;
