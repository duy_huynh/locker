import React, { useContext } from "react";
import callElectron from "src/actions/CallElectron";
import { TYPES } from "src/constants/actions/Control";
import { ControlContext } from "src/context/ControlContext";
import "./style.scss";
const Button = ({ text, href, status }) => {
  const state = useContext(ControlContext);
  const onClick = () => {
    const dispath = state[1];
    dispath({type:TYPES.CALL_SERVICE,pageGo:href,isPush: true, isSending: false});
    callElectron(status);
  };
  return (
    <button className="button" onClick={onClick}>
      {
        //<img width="75" src={src} alt={text} />
      }
      <span>{text}</span>
    </button>
  );
};

export default Button;
