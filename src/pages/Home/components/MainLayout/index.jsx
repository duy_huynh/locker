import React, { useContext, useState } from "react";
import "./style.scss";
import Button from "../Button";
import CONSTANTS, { STATUS } from "src/constants";
import PopUpInfo from "../PopUpInfo";
import { LanguageContext } from "src/context/LanguageContext";

const MainLayout = () => {
  const {dictionary} = useContext(LanguageContext);
  const [showPopUp, setShopPopUp] = useState(false);
  const handleShowPopUp = () => {
    setShopPopUp(true);
  }

  const handleClickAbout = () => {
    setShopPopUp(false);
  }
  return (
    <div className="mainlayout__wrapper">
      <div className="mainlayout__container">
        <div className="mainlayout__container--button">
          <Button
            text={dictionary.HOME.OPEN_LOCKER}
            status={STATUS.DEPOSIT}
            href={CONSTANTS.ROUTERS.PHONE_INPUT}
          ></Button>
          <Button
            text={dictionary.HOME.GET_LOCKER}
            status={STATUS.WITHDRAW}
            href={CONSTANTS.ROUTERS.INPUT_CODE}
          ></Button>
          <div className="mainlayout__info">{showPopUp? <PopUpInfo onClick={handleClickAbout}/>: <></>}<span className="mainlayout__info--icon" onClick={handleShowPopUp}>i</span></div>
        </div>
      </div>
      
    </div>
  );
};

export default MainLayout;
