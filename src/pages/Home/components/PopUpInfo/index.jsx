import React, { useContext } from "react";
import { LanguageContext } from "src/context/LanguageContext";
import "./style.scss";

const PopUpInfo = ({ onClick }) => {
  const { dictionary } = useContext(LanguageContext);
  return (
    <div className="popupinfo__wrapper" onClick={onClick}>
      <span className="popupinfo__shape"></span>
      <span className="popupinfo__shape"></span>
      <div className="popupinfo__container">
        <h3 className="popupinfo__container--title">{dictionary.HOME.TITLE}</h3>
        <ul className="popupinfo__container--list">
          <li>
            {" "}
            <span>{dictionary.HOME.GET}</span>
            <ul>
              <li>
                {dictionary.HOME.LINE11} <span>{dictionary.HOME.LINE12}</span>{" "}
                {dictionary.HOME.LINE13}
              </li>
              <li>{dictionary.HOME.LINE2}</li>
            </ul>
          </li>
          <li>
            <span>{dictionary.HOME.DEPOSIT}</span>
            <ul>
              <li>
                {dictionary.HOME.LINE31} <span>{dictionary.HOME.LINE32}</span>{" "}
                {dictionary.HOME.LINE33}
              </li>
              <li>
                {dictionary.HOME.LINE41} <span>{dictionary.HOME.LINE42}</span>{" "}
                {dictionary.HOME.LINE43}
              </li>
              <li>{dictionary.HOME.LINE5}</li>
              <li>
                <span>{dictionary.HOME.LINE61} </span>
                {dictionary.HOME.LINE62}{" "}
              </li>
              <li>{dictionary.HOME.LINE7}</li>
            </ul>
          </li>

          <li className="call-support">
            <br />
            <span className="need-support">
              {dictionary.HOME.NEED_SUPPORT}
            </span>{" "}
            {dictionary.HOME.CALL_SUPPORT}
          </li>
        </ul>
      </div>
    </div>
  );
};

export default PopUpInfo;
