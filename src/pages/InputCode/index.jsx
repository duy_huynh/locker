import React, { useContext, useEffect, useState } from "react";
import callElectron from "src/actions/CallElectron";
import CONSTANTS, { STATUS } from "src/constants";
import { TYPES } from "src/constants/actions/Control";
import { ControlContext } from "src/context/ControlContext";
import { LanguageContext } from "src/context/LanguageContext";
import ButtonSubmit from "./components/ButtonSubmit";
import InputPhone from "./components/InputPhone";
import KeyBoard from "./components/KeyBoard";
import PopUpInfoInput from "./components/PopUpInfoInput";
import "./style.scss";

const phone_regex = /(([0-9]{6})\b)/g;

const InputCode = () => {
  const [value, setValue] = useState("");
  const [validate, setValidate] = useState(true);
  const controlValue = useContext(ControlContext);
  const [showPopUp, setShopPopUp] = useState(false);
  const [countDown, setCountDown] = useState(60);
  const dispatch = controlValue[1];
  const {dictionary} = useContext(LanguageContext);
  useEffect(() => {
    const myInterval = setInterval(() => {
      if (countDown > 0) {
        setCountDown(countDown - 1);
      } else {
        clearInterval(myInterval);
        callElectron(STATUS.CANCEL);
        dispatch({
          type: TYPES.CALL_SERVICE,
          pageGo: CONSTANTS.ROUTERS.HOME,
          isPush: true,
          isSending: false,
        });
      }
    }, 1000);
    return () => clearInterval(myInterval);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [countDown]);

  const handleGetCode = (newValue = "") => {
    callElectron(`${STATUS.OK} ${newValue===""?value:newValue}`);
    // wait code or error here then redirect or show log
    dispatch({
      type: TYPES.CALL_SERVICE,
      pageGo: CONSTANTS.ROUTERS.LOCKER_NUMBER,
      isPush: true,
      isSending: true,
    });
  };

  const handlePhone = (newNumber) => {
    if (newNumber === undefined || newNumber === "-1") {
      setValidate(true);
      setValue(value.slice(0, -1));
      return null;
    }

    if (value.length >= 6) {
      return null;
    }
    const newValue = `${value}${newNumber}`;
    const newValidate = phone_regex.test(`${value}${newNumber}`);
    if (newValue.length === 6 && newValidate) {
      handleGetCode(newValue);
    }
    if (newValue.length > 6) {
      return null;
    }

    setValue(newValue);

    setValidate(newValidate);
  };

  const handleChangeValue = (e) => {
    const value = e.target.value;
    if (isNaN(value)) {
      return null;
    }
    if (value.length === 6 && validate) {
      handleGetCode();
    }
    if (value.length > 6) {
      return null;
    }

    setValue(value);
    const phone_regex = /(([0-9]{6})\b)/g;
    setValidate(!phone_regex.test(value));
  };

  const handleShowPopUp = () => {
    setShopPopUp(true);
  };

  const handleClickAbout = () => {
    setShopPopUp(false);
  };

  return (
    <div className="inputcode__wrapper">
      <div className="inputcode__info">
        {showPopUp ? <PopUpInfoInput onClick={handleClickAbout} /> : <></>}
        <span className="inputcode__info--icon" onClick={handleShowPopUp}>
          ?
        </span>
      </div>
      <p className="inputcode__countdown">
      {dictionary.INPUT_CODE.TIME} (s):{" "}
        <span className="inputcode__countdown--time">{countDown}</span>
      </p>
      <form className="inputcode__form" onSubmit={(e) => e.preventDefault()}>
        <InputPhone
          title={dictionary.INPUT_CODE.CODE}
          value={value}
          onChange={handleChangeValue}
        ></InputPhone>
        <div className="inputcode__button">
          <ButtonSubmit
            value={dictionary.INPUT_CODE.CANCEL}
            onClick={() => {
              callElectron(STATUS.CANCEL);
              dispatch({
                type: TYPES.CALL_SERVICE,
                pageGo: CONSTANTS.ROUTERS.HOME,
                isPush: true,
                isSending: false,
              });
            }}
          ></ButtonSubmit>
        </div>
        <KeyBoard
          onClick={(e) => {
            handlePhone(e.target.value);
          }}
        />
      </form>
    </div>
  );
};

export default InputCode;
