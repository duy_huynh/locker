import React from "react";
import "./style.scss";

const ButtonOption = ({ value, style = {}, onClick }) => {
  return (
    <button
      value={isNaN(value) ? -1 : value}
      className="buttonoption__wrapper"
      onClick={(e) => onClick(e)}
      style={style}
    >
      {value}
    </button>
  );
};

export default ButtonOption;
