import React from "react";
import ButtonOption from "../ButtonOption";
import { ReactComponent as DeleteIcon } from "./delete.svg";
import "./style.scss";

const KeyBoard = ({ onClick }) => {
  return (
    <div className="keyboard__wrapper">
      <ButtonOption onClick={onClick} value="1" />
      <ButtonOption onClick={onClick} value="2" />
      <ButtonOption onClick={onClick} value="3" />
      <ButtonOption onClick={onClick} value="4" />
      <ButtonOption onClick={onClick} value="5" />
      <ButtonOption onClick={onClick} value="6" />
      <ButtonOption onClick={onClick} value="7" />
      <ButtonOption onClick={onClick} value="8" />
      <ButtonOption onClick={onClick} value="9" />
      <ButtonOption
        onClick={onClick}
        value="0"
        style={{ gridColumnStart: 1, gridColumnEnd: 3 }}
      />
      <ButtonOption
        onClick={onClick}
        value={<DeleteIcon width="30" height="30" />}
      />
    </div>
  );
};

export default KeyBoard;
