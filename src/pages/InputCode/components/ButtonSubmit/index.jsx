import React from "react";
import "./style.scss";

const ButtonSubmit = ({ value, onClick, disabled = false }) => {
  return (
    <button onClick={onClick} disabled={disabled} className="buttonsubmit">
      {value}
    </button>
  );
};

export default ButtonSubmit;
