import React from "react";
import "./style.scss";

const InputPhone = ({ title, value, onChange }) => {
  return (
    <div className="inputphone__wrapper">
      <label className="inputphone__label" htmlFor="phonenumber">
        {title}
      </label>
      <input
        value={value}
        onChange={onChange}
        className="inputphone__phone"
        placeholder=""
      />
    </div>
  );
};

export default InputPhone;
