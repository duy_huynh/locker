import React, { useContext } from "react";
import { LanguageContext } from "src/context/LanguageContext";
import "./style.scss";

const PopUpInfoInput = ({onClick}) => {
  const {dictionary} = useContext(LanguageContext);
  return (
    <div className="popupinfoinput__wrapper" onClick={onClick}>
      <span className="popupinfoinput__shape"></span>
      <span className="popupinfoinput__shape"></span>
      <div className="popupinfoinput__container">
        <h3 className="popupinfoinput__container--title">{dictionary.SUPPORT.SUPPORT}</h3>
        <ul className="popupinfoinput__container--list">
          <li>{dictionary.SUPPORT.INFORMATION_L1}</li>
          <li>{dictionary.SUPPORT.INFORMATION_L2}</li>
          <li>
          {dictionary.SUPPORT.INFORMATION_L3}
          </li>
          <li>
          {dictionary.SUPPORT.INFORMATION_L4}
          </li>
          <li>
            <br />
            {dictionary.SUPPORT.MADE_BY}
          </li>
          <li>{dictionary.SUPPORT.CALL_SUPPORT}</li>
        </ul>
      </div>
    </div>
  );
};

export default PopUpInfoInput;
