import React, { useContext, useEffect, useRef } from "react";
import { useHistory, useParams } from "react-router-dom";
import CONSTANTS from "src/constants";
import { LanguageContext } from "src/context/LanguageContext";
import ContentThank from "./components/ContentThank";
import "./style.scss";

const ThanksPage = () => {
  const { state } = useParams();
  const { dictionary } = useContext(LanguageContext);
  const history = useHistory();
  const ref = useRef(null);
  useEffect(() => {
    ref.current = setTimeout(() => {
       history.push(CONSTANTS.ROUTERS.HOME);
    }, 10000)
    return () => {
      if (ref.current){
        clearTimeout(ref.current);
      }
    }
  }, [history]);

  const redirectHome = () => {
    history.push(CONSTANTS.ROUTERS.HOME);
  }

  return (
    <div onClick={redirectHome}>
      {state === "thanks" ? (
        <ContentThank
          title={dictionary.THANK_PAGE.THANK}
          content={dictionary.THANK_PAGE.TEXT}
        />
      ) : (
        <ContentThank
          title={dictionary.THANK_PAGE.ERROR}
          content={dictionary.THANK_PAGE.ERROR_TEXT}
        />
      )}
    </div>
  );
};

export default ThanksPage;
