import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import CONSTANTS from "src/constants";
import { LanguageContext } from "src/context/LanguageContext";
import BackButton from "src/pages/LockerNumber/components/BackButton";
import Citynow from "../../../../images/logovn.png";
import "./style.scss";

const ContentThank = ({ onClick, content, title }) => {
  const {dictionary} = useContext(LanguageContext);
  const history = useHistory();
  return (
    <div className="contentthank__wrapper" onClick={onClick}>
      <span className="contentthank__shape"></span>
      <span className="contentthank__shape"></span>
      <div className="contentthank__container">
        <img width="200" src={Citynow} alt="Citynow" />
        <h3 className="contentthank__container--title">{title}</h3>
        <ul className="contentthank__container--list">
          <li> {content} </li>
        </ul>
        <BackButton
          onClick={() => history.push(CONSTANTS.ROUTERS.HOME)}
          value={dictionary.THANK_PAGE.BACK_HOME}
        />
      </div>
    </div>
  );
};

export default ContentThank;
