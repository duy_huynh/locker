/* eslint-disable no-case-declarations */
import { TYPES } from "../../constants/actions/Control";

export const initialState = {
  lastReceive: "",
  isSending: false,
  pageGo: "/",
  isPush: false,
  timeReceive: new Date()
};

const reducer = (state, action) => {
  let newState = { ...state };

  switch (action.type) {
    case TYPES.CALL_SERVICE:
      newState.isSending = action.isSending ? true : false;
      newState.pageGo = action.pageGo ? action.pageGo : "/";
      newState.isPush = action.isPush ? true : false;
      return newState;
    case TYPES.RECEIVED_SERVICE:
      newState.isSending =  false;
      newState.lastReceive = action.lastReceive;
      return newState;
    case TYPES.STOP_REDIRECT: 
      newState.isPush = false;
      return newState;
    default:
      return state;
  }
};

export default reducer;
