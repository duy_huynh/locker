/**
 * Locker Project
 * LanguageContext.tsx
 * Copyright © CITYNOW Co. Ltd. All rights reserved.
 */

// Libs
import React, { createContext, useState } from "react";

// others
import { dictionaryList } from "src/languages";
import { CONFIG } from "src/constants/config.js";


export const LanguageContext = createContext({
  userLanguage: "en",
  dictionary: dictionaryList.en,
  userLanguageChange: () => {},
});


/**
 * LanguageProvider
 * @author
 * @since 1.0
 */

export const LanguageProvider = ({ children }) => {
  const [userLanguage, setUserLanguage] = useState("en");

  const provider = {
    userLanguage,
    dictionary: dictionaryList[userLanguage],
    userLanguageChange: async (selected) => {
      const newLanguage = dictionaryList[selected] ? selected : "en";
      setUserLanguage(newLanguage);
      await window.localStorage.setItem(CONFIG.RML_LANGUAGE, newLanguage);
    },
  };

  return (
    <LanguageContext.Provider value={provider}>
      {children}
    </LanguageContext.Provider>
  );
};
