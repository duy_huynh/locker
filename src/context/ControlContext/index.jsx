// Libs
import React, { useReducer } from "react";

export const ControlContext = React.createContext(null);

/**
 * export userprovider to config usercontext
 * @author <name> [<emailAddress>]
 * @since 1.0
 */

export const ControlProvider = ({ reducer, initialState, children }) => (
  <ControlContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </ControlContext.Provider>
);
