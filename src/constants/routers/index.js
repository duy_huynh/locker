const ROUTERS = {
  HOME: "/",
  PHONE_INPUT: "/phoneinput",
  CONFIRM: "/confirm",
  INPUT_CODE: "/inputcode",
  LOCKER_NUMBER: "/lockernumber",
  THANKS_PAGE: "/thankspage"
};

export default ROUTERS;
