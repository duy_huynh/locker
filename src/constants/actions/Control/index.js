const TYPES = {
    CALL_SERVICE: "CALL_SERVICE",
    RECEIVED_SERVICE: "RECEIVED_SERVICE",
    STOP_REDIRECT: "STOP_REDIRECT",
  };
  
export { TYPES };