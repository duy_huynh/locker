import ROUTERS from "./routers";

const CONSTANTS = {
  ENDPOINTS: {},
  ROUTERS,
};

export const STATUS = {
  DEPOSIT: "deposit 0",
  WITHDRAW: "withdraw 0",
  CANCEL: "cancel 0",
  OK: "OK",
}

export default CONSTANTS;