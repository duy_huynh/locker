import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { ControlProvider } from "./context/ControlContext";
import "./index.scss";
import reducer, { initialState } from "./reducer/ControlReducer";

ReactDOM.render(
  <ControlProvider initialState={initialState} reducer={reducer}>
    <App />
  </ControlProvider>,
  document.getElementById("root")
);
