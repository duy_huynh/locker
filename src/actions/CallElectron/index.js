const electron = window.require("electron");
const { ipcRenderer } = electron;

const callElectron = (status) => {
    ipcRenderer.send("SEND_OPIONS", { data: status });
}

export default callElectron;
