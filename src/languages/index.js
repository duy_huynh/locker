/**
 * WORKFLOW-CITYNOW Project
 * languages
 * Copyright © CITYNOW Co. Ltd. All rights reserved.
 */

import en from "./en.json";
import vn from "./vn.json";

/**
 * Language
 * @author <name> [<emailAddress>]
 * @since <Added Date or Version>
 */

export const dictionaryList = { en, vn };

export const languageOptions = [
  {
    icon: "EN",
    lang: "en",
  },
  {
    icon: "VN",
    lang: "vn",
  },
];
