import React, { useContext, useEffect, useState } from "react";
import { ControlContext } from "src/context/ControlContext";
import Loading from "../Loading";

import BG from "src/images/bg.png";
import Citynow from "../../images/logovn.png";
import { TYPES } from "src/constants/actions/Control";
import "./style.scss";
import { useHistory } from "react-router-dom";
import AlertFail from "../AlertFail";
import ListLanguages from "../ListLanguages";
import CONSTANTS from "src/constants";

// Get electron in window to call main process
const electron = window.require("electron");
const { ipcRenderer } = electron;

const WrapperLayout = ({ children }) => {
  const [controlValue, dispatch] = useContext(ControlContext);

  // Using messFail to check has error and shows it
  const [messFail, setMessFail] = useState("");
  const [time, setTime] = useState(new Date().toLocaleTimeString());
  const [timeShow, setTimeShow] = useState(300);
  const history = useHistory();

  useEffect(() => {
    // Listening event
    ipcRenderer.on("MESSAGE_FROM_BACKGROUND_VIA_MAIN", (event, args) => {
      // Set timeshow to default 300
      setTimeShow(300);
      console.log(args);
      // Check message
      if (!args.includes("Wrong") && !args.includes("exist")) {
        setMessFail("");
        dispatch({ type: TYPES.RECEIVED_SERVICE, lastReceive: args });
      } else if (args.includes("exist")) {
        setMessFail("");
        dispatch({
          type: TYPES.CALL_SERVICE,
          isPush: true,
          isSending: false,
          pageGo: `${CONSTANTS.ROUTERS.THANKS_PAGE}/error`,
        });
      } else {
        setMessFail(args);
        dispatch({ type: TYPES.RECEIVED_SERVICE, lastReceive: args });
      }
    });

    // update context is sending
    dispatch({ type: TYPES.CALL_SERVICE, isSending: true });

    // start
    ipcRenderer.send("START_BACKGROUND_VIA_MAIN");

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const intervalTime = setInterval(() => {
      setTime(new Date().toLocaleTimeString());
    }, 1000);
    return () => clearInterval(intervalTime);
  });

  useEffect(() => {
    const myInterval = setInterval(() => {
      if (timeShow > 0) {
        setTimeShow(timeShow - 1);
      }
    }, 1000);

    // Current time

    // Add event to body
    document.body.addEventListener("click", handleClickTimeShow);
    // remove interval and event in body
    return () => {
      clearInterval(myInterval);
      document.body.removeEventListener("click", handleClickTimeShow);
    };
  });

  useEffect(() => {
    if (controlValue.isPush && !controlValue.isSending && messFail === "") {
      dispatch({ type: TYPES.STOP_REDIRECT });
      history.push(controlValue.pageGo);
    }
  }, [controlValue, history, dispatch, messFail]);

  useEffect(() => {});

  const handleClickTimeShow = () => {
    setTimeShow(300);
  };

  return (
    <div className="wrapperlayout__wrapper">
      <div
        className="wrapperlayout__background"
        style={{ backgroundImage: `url(${BG})` }}
      ></div>
      <div className="wrapperlayout__time">
        {" "}
        <input type="checkbox" readOnly checked={timeShow === 0} />{" "}
        {timeShow === 0 ? (
          <iframe
            width="480"
            height="800"
            className="wrapperlayout__time--bg"
            src={`https://www.youtube.com/embed/9BZAs1ETDos?controls=0&loop=1&autoplay=1&playlist=9BZAs1ETDos`}
            title="YouTube video player"
            frameBorder="0"
            allowFullScreen
          ></iframe>
        ) : (
          <></>
        )}
        {/* <video muted loop autoPlay className="wrapperlayout__time--bg">
          <source src="" type="video/mp4" />
          <source src="" type="video/mp4" />
        </video>{" "} */}
        <div className="wrapperlayout__time--count">{time}</div>{" "}
        <img
          className="wrapperlayout__time--logo"
          width="200"
          src={Citynow}
          alt="Citynow"
        />
        <ListLanguages />
      </div>
      {children}
      <Loading isLoading={controlValue.isSending} />
      <AlertFail isFail={messFail !== ""} message={messFail}></AlertFail>
    </div>
  );
};

export default WrapperLayout;
