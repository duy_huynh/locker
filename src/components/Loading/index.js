import React from "react";
import "./style.scss";

const Loading = ({ isLoading=true }) => {
  return isLoading ? <div className="loader__wrapper">
    <div className="loader"></div>
  </div> : <></>;
};

export default Loading;
