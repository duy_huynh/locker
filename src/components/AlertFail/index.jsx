import React from 'react'
import "./style.scss"

const AlertFail = ({isFail=false,message}) => {
    return isFail? (
        <div className="alertfail__wrapper">
            <span className="alertfail__mess">{message}</span>
        </div>
    ):<></>;
};

export default AlertFail
