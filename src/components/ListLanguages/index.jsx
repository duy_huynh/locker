import React, { useContext, useEffect } from "react";
import { CONFIG } from "src/constants/config";
import { LanguageContext } from "src/context/LanguageContext";
import { languageOptions } from "src/languages";
import "./style.scss";

const ListLanguages = () => {
  const { userLanguageChange, userLanguage } = useContext(LanguageContext);
  const handleLanguageChange = (event) =>
  {
    return userLanguageChange ? userLanguageChange(event.target.value) : null;
  }

  useEffect(() => {
    const userLanguage = async () => {
      let defaultLanguage = await window.localStorage.getItem(
        CONFIG.RML_LANGUAGE
      );
      if (!defaultLanguage) {
        defaultLanguage = window.navigator.language.substring(0, 2);
      }
      if (userLanguageChange) {
        userLanguageChange(defaultLanguage);
      }
    };
    userLanguage();
  }, [userLanguageChange]);

  return (
    <select className="listlanguages__wrapper" value={userLanguage} onChange={handleLanguageChange}>
      {languageOptions.map((e) => (
        <option key={e.lang} value={e.lang}>{e.icon}</option>
      ))}
    </select>
  );
};

export default ListLanguages;
